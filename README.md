# gameamea-switchvscode README

Extension for VS code to manage the switchVScode Script shell.

## Features

TODO

## Requirements

Need the [switchVsCode shell script](https://gitlab.com/linuxdev_gameamea/osscripts/-/blob/master/Linux/switchVsCode)
Note that this script must be edited to match your work environment.

## Extension Settings

TODO

## Known Issues

TODO

## Release Notes

see [Changelog](CHANGELOG.md)

-----------------------------------------------------------------------------------------------------------