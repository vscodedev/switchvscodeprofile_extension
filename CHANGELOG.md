# Change Log

All notable changes to the "gameamea-switchvscode" extension will be documented in this file.

This file follows recommendations of [Keep a Changelog](http://keepachangelog.com/).

## [Unreleased]

## [0.1.0] - 2020-11-23

### Added

- Initial release

### Changed

- None

### Removed

- None